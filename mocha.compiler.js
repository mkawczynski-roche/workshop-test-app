require('babel-core/register');
require('babel-polyfill');

var storage = {};

/*
 * Local storage mock for tests
 */
global.localStorage = {
  setItem: function(key, item){
    storage[key]=item;
  },
  getItem: function(key){
    return storage[key] === void 0 ? null : storage[key];
  }
};

