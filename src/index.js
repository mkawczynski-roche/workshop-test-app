import {saveScore, getScorers, getTopScorers} from './local-storage-service';


saveScore('test', 67).then(() => {
  getScorers().then(scorers => console.log(scorers));
  getTopScorers().then(scorers => console.log(scorers));
});


export function toTest() {
  return true;
}

