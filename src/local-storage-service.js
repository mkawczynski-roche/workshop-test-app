let STORAGE_KEY = 'hunt-game';

const DELAY = 1 * 100;
const TOP_LIMIT = 5;
const CONFIG_STORAGE = 'db';


var dbStorage = {};

/*
 * Local storage mock for tests
 */
var db = {
    insert: function(key, item){
        dbStorage[key]=item;
    },
    select: function(key){
        return dbStorage[key] === void 0 ? null : dbStorage[key];
    }
};


const storageStrategies = {
    local: {set: null, get: null},
    db: {set: null, get: null}
};

const storage = storageStrategies[CONFIG_STORAGE];


(function () {
  storageStrategies.local.set = (key, item) => {
      console.log("set - local");
    return new Promise(resolve => {
      setTimeout(() => {
          localStorage.setItem(key, item);
        resolve();
      }, DELAY);
    });
  };
})();

(function () {
  storageStrategies.local.get = (key) => {
      console.log("get - local");
    return new Promise(resolve => {
      setTimeout(() => {
        const result = localStorage.getItem(key);
        resolve(result);
      }, DELAY);
    });
  };
})();

(function () {
    storageStrategies.db.set = (key, item) => {
        console.log("set - db");
        return new Promise(resolve => {
            setTimeout(() => {
                db.insert(key, item);
                resolve();
            }, DELAY);
        });
    };
})();

(function () {
    storageStrategies.db.get = (key) => {
        console.log("get - db");
        return new Promise(resolve => {
            setTimeout(() => {
                const result = db.select(key);
                resolve(result);
            }, DELAY);
        });
    };
})();

export function saveScore(name, score) {
  return new Promise(resolve => {
    getScorers().then((scorers) => {
      scorers[name] = Math.max(score, scorers[name] || score);
      const promise = storage.set(STORAGE_KEY, JSON.stringify(scorers));
      promise.then(() => resolve());
    });

  });
}

export function getScorers() {
  return new Promise(resolve => {
    storage.get(STORAGE_KEY).then(data => {
      resolve(data !== null ? JSON.parse(data) : {});
    });
  });
}

export function getTopScorers() {
  return new Promise(resolve => {
    getScorers().then(scorers => {

        const topScorers = Object.keys(scorers)
              .map(name => ({name, score: scorers[name]}))
              .sort((a, b) => b.score - a.score)
              .slice(0, TOP_LIMIT);
      resolve(topScorers);
    });
  });
}