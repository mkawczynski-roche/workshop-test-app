import expect from 'expect';

import {toTest} from '../src/index';


describe('#index', () => {

  it('should return true', () => {
    expect(toTest()).toEqual(true);
  });

});